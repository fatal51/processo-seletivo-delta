import React from "react";
import Routes from "./routes.js";
import "./index.css";

function App() {
  return (
    <div>
      <Routes></Routes>
    </div>
  );
}

export default App;
