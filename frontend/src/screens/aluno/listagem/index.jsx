import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import "./index.css";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Table,
  DropdownButton,
  Button,
  Dropdown,
  ButtonGroup,
  Modal,
  InputGroup,
  FormControl,
} from "react-bootstrap";
import { getAlunos, deleteAluno } from "../../../services/api";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import { Create, DeleteOutline, Visibility } from "@material-ui/icons";
import GoBack from "../../menu/goBack";

function Listagem() {
  const history = useHistory();
  const [campoPesquisa, setCampoPesquisa] = useState("");
  const [alunos, setAlunos] = useState([]);
  const [show, setShow] = useState(false);
  const [fotoUsuario, setFotoUsuario] = useState("");

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    findAlunos();
  }, []); // eslint-disable-line

  function goToCadastro() {
    history.push("/cadastro");
  }

  async function excluirAluno(idAluno) {
    confirmAlert({
      title: "Exclusão de Aluno",
      message: "Tem certeza que deseja excluir esse aluno?",
      buttons: [
        {
          label: "Sim",
          onClick: async () => {
            await deleteAluno(idAluno);
            findAlunos();
          },
        },
        {
          label: "Não",
          onClick: () => {},
        },
      ],
    });
  }

  function findAlunos() {
    getAlunos(campoPesquisa).then((alunos) => setAlunos(alunos));
  }

  function visualizarFoto(foto) {
    setFotoUsuario(foto);
    handleShow();
  }

  function editar(idAluno) {
    history.push(`/editar/${idAluno}`);
  }

  return (
    <div className="container-listagem">
      <div className="table-listagem">
        <GoBack link={"/"} />
        <Button
          className="mb-1 float-left"
          type="button"
          onClick={goToCadastro}
        >
          <i className="fa fa-plus"></i> Aluno
        </Button>
        <table class="table">
          <thead>
            <tr>
              <th className="sizeColButton">Foto</th>
              <th>Nome</th>
              <th>Endereço</th>
              <th className="sizeColButton">Ações</th>
            </tr>
          </thead>
          <tbody>
            {alunos.length > 0 ? (
              alunos.map((aluno) => {
                return (
                  <tr key={aluno.id}>
                    <td>
                      <span className="flex-center">
                        <img
                          style={{
                            width: 50,
                            height: 50,
                          }}
                          src={`http://matheus.tplinkdns.com:8080/api/documento/download/${aluno.foto}`}
                          alt="fotoAluno"
                        ></img>
                      </span>
                    </td>
                    <td>{aluno.nome}</td>
                    <td>{aluno.endereco}</td>
                    <td className="flex-center">
                      <button
                        className="btn btn-success btn-sm mr-1"
                        onClick={() => editar(aluno.id)}
                      >
                        <i className="fa fa-pencil" />
                      </button>
                      <button
                        className="btn btn-danger btn-sm"
                        onClick={() => excluirAluno(aluno.id)}
                      >
                        <i className="fa fa-trash" />
                      </button>
                    </td>
                  </tr>
                );
              })
            ) : (
              <tr>
                <td colSpan="4">
                  <span className="center">Nenhum aluno encontrado</span>
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default Listagem;
