import { useParams } from "react-router-dom";
import React, { useState, useEffect, useRef } from "react";
import "./index.css";
import { getAluno, uploadFoto, saveAluno } from "../../../services/api";
import { useHistory } from "react-router-dom";
import ImageUploader from "react-images-upload";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import GoBack from "../../menu/goBack";

const alunoSshema = Yup.object().shape({
  nome: Yup.string().min(4, "Poucos caracteres").required("Obrigatório"),
  endereco: Yup.string().min(6, "Poucos caracteres").required("Obrigatório"),
  foto: Yup.string().min(6, "Poucos caracteres").required("Insira uma imagem"),
});

const Aluno = () => {
  toast.configure({
    autoClose: 5000,
    draggable: false,
  });

  const formikRef = useRef();

  const { idAluno } = useParams();
  const [editando, setEditando] = useState(false);
  const [aluno, setAluno] = useState({
    id: null,
    nome: "",
    endereco: "",
    foto: "",
  });

  useEffect(() => {
    if (idAluno) {
      getAluno(idAluno).then((aluno) => {
        formikRef.current.values.id = aluno.id;
        formikRef.current.values.nome = aluno.nome;
        formikRef.current.values.endereco = aluno.endereco;
        formikRef.current.values.foto = aluno.foto;
        setAluno(aluno);
        setEditando(true);
      });
    }
  }, [idAluno]);

  const uploadFotoMethod = (part, setFieldValue) => {
    const formData = new FormData();
    formData.append("file", part[0]);
    uploadFoto(formData).then((response) => {
      setFieldValue("foto", response.data);
    });
  };

  const history = useHistory();
  async function cadastrar(alunoForm) {
    saveAluno(alunoForm).then(() => {
      history.push("/listagem");
    });
  }

  return (
    <div className="container box-shadow mt-5">
      <div className="row">
        <GoBack link={"/"} />
        <div className="col-12">
          {!editando ? <h3>Cadastro de aluno</h3> : <h3>Editar de aluno</h3>}
        </div>
      </div>
      <Formik
        initialValues={aluno}
        innerRef={formikRef}
        validationSchema={alunoSshema}
        onSubmit={(values) => {
          cadastrar(values);
        }}
      >
        {({ errors, touched, values, setFieldValue, isValid }) => (
          <Form>
            <div className="col-lg-12">
              <div className="container-fluid">
                <div className="row">
                  <label className="col-lg-6">
                    Nome *
                    <Field
                      type="text"
                      id="inputBairro"
                      className={`form-control ${
                        errors.nome && touched.nome ? "invalid" : ""
                      }`}
                      name="nome"
                    />
                    {errors.nome && touched.nome ? (
                      <span className="text-error">{errors.nome}</span>
                    ) : null}
                  </label>
                  <label className="col-lg-6">
                    Endereço *
                    <Field
                      type="text"
                      id="inputBairro"
                      className={`form-control ${
                        errors.endereco && touched.endereco ? "invalid" : ""
                      }`}
                      name="endereco"
                    />
                    {errors.endereco && touched.endereco ? (
                      <span className="text-error">{errors.endereco}</span>
                    ) : null}
                  </label>
                  <label className="col-lg-12">
                    {values.foto && values.foto !== "" ? (
                      <>
                        <img
                          className="img-fluid mx-auto d-block"
                          src={`http://matheus.tplinkdns.com:8080/api/documento/download/${values.foto}`}
                        />
                        <button
                          type="button"
                          className="btn btn-sm btn-danger btn-foto-aluno"
                          onClick={() => setFieldValue("foto", "")}
                        >
                          <i className="fa fa-close"></i>
                        </button>
                      </>
                    ) : (
                      <>
                        <ImageUploader
                          className="box-shadow"
                          withIcon={true}
                          onChange={(pictureFiles) =>
                            uploadFotoMethod(pictureFiles, setFieldValue)
                          }
                          imgExtension={[".jpg"]}
                          maxFileSize={2242880}
                          singleImage={true}
                          buttonText="Selecione imagem"
                          label="Tamanho maximo: 2mb, formatos: jpg"
                          fileTypeError=" não é suportado"
                          fileSizeError="Imagem muito grande, maximo 2MB"
                        />
                        {errors.foto && touched.foto ? (
                          <span className="text-error">{errors.foto}</span>
                        ) : null}
                      </>
                    )}
                  </label>
                </div>
                <div className="row">
                  <div className="col-4">
                    <button
                      type="submit"
                      onClick={() => {
                        if (!isValid) {
                          toast.warning("Preencha todos os campos destacados");
                        }
                      }}
                      className="btn btn-success mb-3 mr-3"
                    >
                      <i className="pi pi-save"></i> Salvar
                    </button>
                    <button type="reset" className="btn btn-danger mb-3">
                      <i className="pi pi-times"></i> Cancelar
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default Aluno;
