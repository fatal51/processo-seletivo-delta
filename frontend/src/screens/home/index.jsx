import React from "react";
import { Link } from "react-router-dom";

import "./index.css";

const menu = [
  { icone: "fa-user", titulo: "Cadastrar Alunos", uri: "/cadastro" },
  { icone: "fa-list", titulo: "Lista de alunos", uri: "/listagem" },
];

const Card = (props) => {
  return (
    <div className="col-lg-3 col-md-4 col-sm-6 mb-2">
      <Link className="no-decoration" to={props.uri}>
        <div className="card text-center pt-5 pb-5">
          <i className={`fa-icone-card fa ${props.icone}`}></i>
          <span>{props.titulo}</span>
        </div>
      </Link>
    </div>
  );
};

const Home = () => {
  return (
    <div className="container">
      <div className="row mt-5 justify-content-around">
        {menu.map((modulo) => {
          return <Card key={modulo.titulo} {...modulo} />;
        })}
      </div>
    </div>
  );
};

export default Home;
