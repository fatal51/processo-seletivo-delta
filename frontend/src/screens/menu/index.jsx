import React from "react";
import "./index.css";
import "bootstrap/dist/css/bootstrap.min.css";

const Menu = () => {
  return (
    <div className="container-fluid container-menu box-shadow">
      <a className="icon" href="/">
        Processo seletivo Delta
      </a>
    </div>
  );
};

export default Menu;
