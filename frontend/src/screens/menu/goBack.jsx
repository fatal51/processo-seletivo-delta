import React from "react";
import "./index.css";
import { Link } from "react-router-dom";

const GoBack = (props) => {
  return (
    <div className="go-back">
      <Link to={props.link}>
        <i className="fa fa-arrow-left" />
      </Link>
    </div>
  );
};

export default GoBack;
